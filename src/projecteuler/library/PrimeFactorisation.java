/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecteuler.library;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Peter
 */
public class PrimeFactorisation {
 
    private int[] primes;
    private List<Integer> factors = new ArrayList();

    public PrimeFactorisation() {
        
        List<Integer> primes1 = readPrimes("C:\\Users\\Peter\\Documents\\NetBeansProjects\\ProjecEuler\\src\\projecteuler\\library\\primes1.txt");
        List<Integer> primes2 = readPrimes("C:\\Users\\Peter\\Documents\\NetBeansProjects\\ProjecEuler\\src\\projecteuler\\library\\primes2.txt");
    
        primes1.addAll(primes2);
        primes = primes1.stream().mapToInt(i -> i).toArray();
    }

    
    
    private void run() {
        double targetNo = 99.0;
        factorise(targetNo, 0);
        factors.stream().forEach(n -> System.out.println("Factors of " + targetNo + " = " + n));
    }

    public Double factorise(Double targetNo, int primeIndex) {
        
        if (isPrime(targetNo)) {
            factors.add(targetNo.intValue());
            return targetNo;
        }
      
        Double aPrime = (double)primes[primeIndex];
        if (isDivisibleBy(targetNo, aPrime)) {
            targetNo = targetNo / aPrime;
            factors.add(aPrime.intValue());
            factorise(targetNo, 0);
        } else {
            factorise(targetNo, primeIndex + 1);
        }

        return aPrime;
    }

    private boolean isDivisibleBy(Double targetNo, Double aPrime) {
        return getFractionalPart(targetNo / aPrime) == 0.0d;
    }
    
    private double getFractionalPart(double n) {
        if (n > 0) {
            return n - Math.floor(n);
        } else {
            return ((n - Math.ceil(n)) * -1);
        }
    }

    private boolean isPrime(double targetNo) {
        return Arrays.stream(primes).anyMatch(p-> p == targetNo);
    }

    public boolean isPrime(int number) {
        for(int check = 2; check < number; ++check) {
            if(number % check == 0) {
                return false;
            }
        }
        return true;
    }

    public List<Integer> getFactors() {
        return factors;
    }

   
    
    public static void main(String[] args) {
        new PrimeFactorisation().run();
        
    }

    public void clearFactors() {
     this.factors = new ArrayList();
    }

    private List<Integer> readPrimes(String fileName) {

        List<String> list = new ArrayList<>();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
            list = br.lines().skip(2).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //list.forEach(System.out::println);
        return list.stream()
                .map(line -> split(line))
                .flatMap(l -> l.stream())
                .collect(Collectors.toList());
               
                
                
    }
    
    private List<Integer> split(String line){
        return Stream.of(line.split("\\s+"))
                .filter(l -> l.trim().length() > 0)
                .map(s -> Integer.parseInt(s))
                .collect(Collectors.toList());
               
    }
}
