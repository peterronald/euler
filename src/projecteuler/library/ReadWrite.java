/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecteuler.library;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Peter
 */
public class ReadWrite<S,T> {

    public <S,T> HashMap<S,T> read(Map<S, T> map,String location) {
        HashMap<S, T> mapInFile = null;
        try {
            File toRead = new File(location);
            FileInputStream fis = new FileInputStream(toRead);
            ObjectInputStream ois = new ObjectInputStream(fis);

            mapInFile = (HashMap<S, T>) ois.readObject();

            ois.close();
            fis.close();
        } catch (Exception e) {
        }
        return mapInFile;
    }

    public <S,T>  void write(Map<S, T> map, String location) {
        try {
            File fileOne = new File(location);
            FileOutputStream fos = new FileOutputStream(fileOne);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(map);
            oos.flush();
            oos.close();
            fos.close();
        } catch (Exception e) {
        }

    }
}
