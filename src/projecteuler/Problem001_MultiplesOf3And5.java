/*
Multiples of 3 and 5
Problem 1

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
 */
package projecteuler;

import java.util.function.Predicate;
import java.util.stream.IntStream;

/**
 *
 * @author Peter Ronald
 * 
 */
public class Problem001_MultiplesOf3And5 {
   
    public static void main(String[] args) {
        new Problem001_MultiplesOf3And5().run();
    }

    private void run() {
        
        Predicate<Integer> isDivisibleBy3 = (x) -> x % 3 == 0;
        Predicate<Integer> isDivisibleBy5 = (x) -> x % 5 == 0;
         
        int sum = IntStream.range(1, 1000).boxed()
                .filter(isDivisibleBy3.or(isDivisibleBy5))
                .mapToInt(x->x)
                .sum();
        System.out.println("Sum of all the multiples of 3 or 5 below 1000 = " + sum);
    }
}
