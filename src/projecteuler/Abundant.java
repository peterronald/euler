/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecteuler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 2. Write a Java program to classify Abundant, deficient and perfect number
 * (integers) between 1 to 10,000. Go to the editor
 *
 * In number theory, an abundant number is a number for which the sum of its
 * proper divisors is greater than the number itself. Example : The first few
 * abundant numbers are: 12, 18, 20, 24, 30, 36, 40, 42, 48, 54, 56, 60, 66, 70,
 * 72, 78, 80, 84, 88, 90, 96, 100, 102,… The integer 12 is the first abundant
 * number. Its proper divisors are 1, 2, 3, 4 and 6 for a total of 16. Deficient
 * number: In number theory, a deficient number is a number n for which the sum
 * of divisors σ(n)<2n, or, equivalently, the sum of proper divisors (or aliquot
 * sum) s(n)<n. The value 2n − σ(n) (or n − s(n)) is called the number's
 * deficiency. As an example, divisors of 21 are 1, 3 and 7, and their sum is
 * 11. Because 11 is less than 21, the number 21 is deficient. Its deficiency is
 * 2 × 21 − 32 = 10. The first few deficient numbers are: 1, 2, 3, 4, 5, 7, 8,
 * 9, 10, 11, 13, 14, 15, 16, 17, 19, 21, 22, 23, 25, 26, 27, 29, 31, 32, 33,
 * ……. Perfect number: In number system, a perfect number is a positive integer
 * that is equal to the sum of its proper positive divisors, that is, the sum of
 * its positive divisors excluding the number itself. Equivalently, a perfect
 * number is a number that is half the sum of all of its positive divisors
 * (including itself) i.e. σ1(n) = 2n. The first perfect number is 6. Its proper
 * divisors are 1, 2, and 3, and 1 + 2 + 3 = 6. Equivalently, the number 6 is
 * equal to half the sum of all its positive divisors: ( 1 + 2 + 3 + 6 ) / 2 =
 * 6. The next perfect number is 28 = 1 + 2 + 4 + 7 + 14. This is followed by
 * the perfect numbers 496 and 8128. Expected Output : Number Counting
 * [(integers) between 1 to 10,000]: Deficient number: 7508 Perfect number: 4
 * Abundant number: 2488
 */
public class Abundant {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        countNumberTypes(createDivisorMap());
    }

    private static Map<Integer, List<Integer>> createDivisorMap() {
        Map<Integer, List<Integer>> divisorMap = new HashMap();
        for (int i = 1; i <= 10000; i++) {
            divisorMap.put(i, findDivisors(i));
        }
        return divisorMap;
    }

    private static List<Integer> findDivisors(int number) {
        List<Integer> divisors = new ArrayList();
        for (int i = 1; i < number; i++) {
            //System.out.println("i= " + i + " number = "  + number  + " modulo = " +  (number % i)  );

            if (number % i == 0) {
                divisors.add(i);
            }
        }
        return divisors;
    }

    private static void countNumberTypes(Map<Integer, List<Integer>> divisorMap) {
        int abundantCount = 0;
        int deficientCount = 0;
        int perfectCount = 0;
        for (Map.Entry<Integer, List<Integer>> entry : divisorMap.entrySet()) {
            Integer number = entry.getKey();
            List<Integer> values = entry.getValue();

            int sum = 0;
            for (Integer value : values) {
                sum += value;
            }

            if (sum > number) {
                abundantCount++;
            } else if (sum < number) {
                deficientCount++;
            } else if (sum == number) {
                perfectCount++;
            } else {

            }

        }
        System.out.println("Integer count " + divisorMap.size());
        System.out.println("abundant count " + abundantCount);
        System.out.println("deficient count " + deficientCount);
        System.out.println("perfect count " + perfectCount);
    }

}
