/*
Lattice paths
Problem 15

Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, 
there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20×20 grid?
 */
package projecteuler;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;

/**
 *
 * @author Peter
 */
public class Problem015_LatticePaths {

    public static void main(String[] args) {

//        long gridSize = 20;
//        Problema15 p15 = new Problema15();
//        p15.findRoutes(gridSize);
//        System.out.print("Routes Found:" + p15.routesFound);

        new Problem015_LatticePaths().runIt();   
    }

    private void run() {

        final int MOVES = 39;

        // Create first routes.
        Route route1 = new Route("r", 19, 20);
        Route route2 = new Route("d", 20, 19);

        // Add to routes to start level.
        List<Route> startLevel = new ArrayList();
        startLevel.add(route1);
        startLevel.add(route2);

        // Find all routes to level.
        List<Route> finalLevel = doLevel(startLevel, MOVES);

        //System.out.println("Paths = " + finalLevel.size());
        finalLevel.forEach(r -> {
            System.out.println("Route = " + r.path);
        });

    }

    private List<Route> doLevel(List<Route> thisLevel, int levels) {
        if (levels > 0) {
            List<Route> nextLevel = generateNextLevel(thisLevel);
            thisLevel = doLevel(nextLevel, levels - 1);
        }
        return thisLevel;
    }

    private List<Route> generateNextLevel(List<Route> routes) {

        List<Route> nextLevel = new ArrayList();
        for (Route route : routes) {

            // Add right route.
            if (route.remainingR != 0) {
                nextLevel.add(new Route(route.path + "r", route.remainingR - 1, route.remainingD));
            }

            // Add down route.
            if (route.remainingD != 0) {
                nextLevel.add(new Route(route.path + "d", route.remainingR, route.remainingD - 1));
            }
        }
        return nextLevel;
    }

    private void runIt() {
        BigInteger fact40 = factorial(BigInteger.valueOf(40));
        BigInteger fact20 = factorial(BigInteger.valueOf(20));
        BigInteger ways = (fact40.divide(fact20)).divide(fact20);

        System.out.println(ways);
    }

    public static BigInteger factorial(BigInteger n) {
        BigInteger factorial = BigInteger.valueOf(1);
        if (n.equals(BigInteger.valueOf(0))) {
            return factorial;
        }

        for (int i = 1; i <= n.intValue(); i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }

        return factorial;
    }
}

class Route {

    String path = "";
    int remainingR = 0;
    int remainingD = 0;

    public Route(String path, int remainingR, int remainingD) {
        this.path = path;
        this.remainingR = remainingR;
        this.remainingD = remainingD;
    }

}

class Problema15 {

    public static long routesFound = 0;
    public static long gridSize = 0;

    public long findRoutes(long size) {
        routesFound = 0;
        gridSize = size;

        checkRoute(0, 0);

        return routesFound;
    }

    public synchronized void increaseRoutesFound() {
        routesFound++;
    }

    private void checkRoute(long i, long j) {
        if (i == gridSize && j == gridSize) {
            increaseRoutesFound();
        } else {

            if (j < (gridSize + 1)) { // East
                checkRoute(i, j + 1);
            }

            if (i < (gridSize + 1)) { // South
                checkRoute(i + 1, j);
            }
        }
    }

}
