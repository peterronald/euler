/*
Summation of primes
Problem 10

The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

 */
package projecteuler;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Peter
 */
public class Problem010_SummationOfPrimes {

    public static void main(String[] args) {
        new Problem010_SummationOfPrimes().run();
    }

    private void run() {
        List<Long> primes = findSeivePrimes(1999999);
        Long sum = primes.stream().reduce(0l, (a,b) -> a+b);
        System.out.println("Sum of all the primes below two million = " + sum);
    }

    public List<Long> findSeivePrimes(int primesUpToNum) {
        List<Long> marked = new ArrayList();
        List<Long> primes = new ArrayList();

        for (long i = 2; i < Math.sqrt((double) primesUpToNum); i++) {
            if (!marked.contains(i)) {
                primes.add(i);
            }
            for (long j = i; j < primesUpToNum; j += i) {
                marked.add(j);
            }
        }
        return primes;
    }

}
