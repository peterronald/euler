/*
Number letter counts
Problem 17

If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 
115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.

 */
package projecteuler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *
 * @author Peter
 */
public class Problem017_NumberLetterCounts {

    public static void main(String[] args) {
        new Problem017_NumberLetterCounts().run();
    }

    private void run() {

        Stream<String> words = IntStream.rangeClosed(1, 1000).boxed()
                .map(i -> createWords(i));
        words.forEach(w -> System.out.println(w + "  " + countLength(w)));

        long sum = IntStream.rangeClosed(1, 1000).boxed()
                .map(i -> createWords(i))
                .mapToLong(w -> countLength(w))
                .sum();

        System.out.println("If all the numbers from 1 to 1000 (one thousand) inclusive were written "
                + "out in words, how many letters would be used? = " + sum);

    }
    
    private long countLength(String word) {
        return word.chars()
                .filter(c -> !Character.isWhitespace(c))
                .count();
    }

    private String createWords(int num) {
        
        List<Integer> parts = setParts(num);
        int hundreds= parts.get(0); 
        int tens = parts.get(1);
        int units = parts.get(2);

        String and = " and ";
        String space = " ";
        String hundred = " hundred";
        String str = "";
        int strLength = String.valueOf(num).length();

        switch (strLength) {
            case 1:
                str = figures().get(num);
                break;

            case 2:
                if (tens == 1) {
                    str = figures().get(tens * 10 + units);
                } else {
                    str = figures().get(tens * 10) + space + figures().get(units);
                }
                break;

            case 3:
                if (tens == 0 && units == 0) {
                    str = figures().get(hundreds) + hundred;
                } else if (units == 0) {
                    str = figures().get(hundreds) + hundred + and + figures().get(tens * 10);
                } else if (tens == 1) {
                    str = figures().get(hundreds) + hundred + and + figures().get(tens * 10 + units);
                } else {
                    str = figures().get(hundreds) + hundred + and + figures().get(tens * 10) + space + figures().get(units);
                }
                break;
            case 4:
                str = "one thousand";
                break;

        }
        return str;
    }

    private List<Integer> setParts(int num) {
        List<Integer> parts = new ArrayList();
        int hundreds = 0;
        int tens = 0;
        int units = 0;

        hundreds = num / 100;
        if (hundreds > 0) {
            num = num - hundreds * 100;
        }
        tens = num / 10;
        units = num % 10;

        parts.add(hundreds);
        parts.add(tens);
        parts.add(units);
        return parts;
    }

    private static Map<Integer, String> figures() {
        Map<Integer, String> result = new HashMap();
        result.put(0, "");
        result.put(1, "one");
        result.put(2, "two");
        result.put(3, "three");
        result.put(4, "four");
        result.put(5, "five");
        result.put(6, "six");
        result.put(7, "seven");
        result.put(8, "eight");
        result.put(9, "nine");
        result.put(10, "ten");
        result.put(11, "eleven");
        result.put(12, "twelve");
        result.put(13, "thirteen");
        result.put(14, "fourteen");
        result.put(15, "fifteen");
        result.put(16, "sixteen");
        result.put(17, "seventeen");
        result.put(18, "eighteen");
        result.put(19, "nineteen");
        result.put(20, "twenty");
        result.put(30, "thirty");
        result.put(40, "forty");
        result.put(50, "fifty");
        result.put(60, "sixty");
        result.put(70, "seventy");
        result.put(80, "eighty");
        result.put(90, "ninety");
        return Collections.unmodifiableMap(result);
    }

}
