/*
Amicable chains
Problem 95

The proper divisors of a number are all the divisors excluding the number itself. For example, the proper divisors of 28 are 1, 2, 4, 7, and 14. As the sum of these divisors is equal to 28, we call it a perfect number.

Interestingly the sum of the proper divisors of 220 is 284 and the sum of the proper divisors of 284 is 220, forming a chain of two numbers. For this reason, 220 and 284 are called an amicable pair.

Perhaps less well known are longer chains. For example, starting with 12496, we form a chain of five numbers:

12496 → 14288 → 15472 → 14536 → 14264 (→ 12496 → ...)

Since this chain returns to its starting point, it is called an amicable chain.

Find the smallest member of the longest amicable chain with no element exceeding one million.

 */
package projecteuler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import projecteuler.library.PrimeFactorisation;
import projecteuler.library.ReadWrite;

/**
 *
 * @author Peter
 */
public class Problem095_AmicableChains {

    PrimeFactorisation pf = new PrimeFactorisation();

    private void run() {

        // Make factors map.
        Map<Integer, List<Integer>> primeFactors = 
                IntStream.rangeClosed(2, 1000000).boxed()
                       .collect(Collectors.toMap(Function.identity(), n -> this.getFactors(n))
                );
//        primeFactors.entrySet().forEach(entry -> {
//                System.out.println("entry " + entry.getKey()  +  "  " + entry.getValue());
//            });

        // Make sum of Factors map.
        Map<Integer, Integer> sumOfFactors
                = primeFactors.entrySet().stream()
                        .collect(Collectors.toMap(entry -> entry.getKey(), entry -> sumFactors(entry.getValue())));
        //System.out.println(sumOfFactors.toString());
        
        ReadWrite<Integer,Integer> readWrite = new ReadWrite();
        readWrite.write(sumOfFactors, SUM_LOCATION);;
        
        HashMap<Integer, Integer> readMap = readWrite.read(sumOfFactors, SUM_LOCATION);
        readMap.entrySet().forEach(entry -> {
                System.out.println(entry.getKey()  +  ":" + entry.getValue());
         });
    }
    private static final String SUM_LOCATION = "C:\\Users\\Peter\\Documents\\NetBeansProjects\\ProjecEuler\\src\\projecteuler\\library\\sumofFactors.txt";

    private Integer sumFactors(List<Integer> primeFactors){
        List<List<Integer>> mainList = new ArrayList();
        
        // Find distinct numbers [2,3].
        List<Integer> distinctFactors = primeFactors.stream()
                                            .distinct()
                                            .collect(Collectors.toList());
        distinctFactors.forEach((factor) -> {
            
            // Separate out numbers [2, 2][3].
            List<Integer> distinctList = primeFactors.stream()
                                .filter(e -> e.equals(factor))
                                .collect(Collectors.toList());
            distinctList.add(factor);
            
            // Create powers [1,2,4][1,9].
            List<Integer> powerList = 
                    IntStream.range(0, distinctList.size())
                    .mapToObj(i -> (int) Math.pow((double) distinctList.get(i), i))
                    .collect(Collectors.toList());

            mainList.add(powerList);
        });
        
        // Make the dot product to find factors, sum them.
        Integer sumOfFactors = 0;
        Optional<List<Integer>> factors = findFactors(mainList);
        if (factors.isPresent()) {
            OptionalInt max = factors.get().stream().mapToInt(Integer::intValue).max();
            factors.get().removeIf(x -> x == max.getAsInt());
            sumOfFactors = factors.get().stream().reduce(0, Integer::sum);
        }
        
        return sumOfFactors;
        
    }
    
    
    private Optional<List<Integer>> findFactors(List<List<Integer>> list) {
        Optional<List<Integer>> factors = list.stream()
                .reduce((a,b) -> dotProduct(a,b));
        return factors;
    }

    private List<Integer> dotProduct(List<Integer> aList, List<Integer> bList) {
        List<Integer> cartesianProduct = new ArrayList();
        aList.forEach(a -> bList.forEach(b -> cartesianProduct.add(a * b)));        
        return cartesianProduct;
    }
    
    private List<Integer> getFactors(int n) {
        pf.clearFactors();
        pf.factorise((double) n, 0);
        //System.out.println(n + ":" + pf.getFactors());
        return pf.getFactors();

    }

    public static void main(String[] args) {

        new Problem095_AmicableChains().run();

//        List<Integer> factors = new Problem095_AmicableChains().getFactors(30010);
//        System.out.println("projecteuler.Problem095_AmicableChains.main()" +  factors.toString());
    }

}
