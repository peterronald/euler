/*
Smallest multiple
Problem 5

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

 */
package projecteuler;

import java.util.OptionalInt;
import java.util.stream.IntStream;

/**
 *
 * @author Peter
 */
public class Problem005_SmallestMultiple {
    
     public static void main(String[] args) {
        new Problem005_SmallestMultiple().run();
    }

    private void run() {
        

         OptionalInt findFirst = IntStream.rangeClosed(1, 1000000000)
                 .filter(i ->
                         IntStream.rangeClosed(1, 20)
                                 .noneMatch(j -> i % j != 0)
                                
                 )
                 .findFirst();
               
         
        System.out.println("Smallest positive number that is evenly divisible by all of the numbers from 1 to 20 = " + findFirst.getAsInt());                
    }
                       

}

    
    
