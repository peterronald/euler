/*
Largest prime factor
Problem 3

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?

 */
package projecteuler;

import java.util.stream.IntStream;

/**
 *
 * @author Peter
 */
public class Problem003_LargestPrimeFactor {


    double[] primes;
    
    public static void main(String[] args) {
        new Problem003_LargestPrimeFactor().run();
    }

    private void run() {
        double targetNo = 600851475143d;

        createPrimes();
        primeFactorise(targetNo, 0);
    }

    private Double primeFactorise(Double targetNo, int primeIndex) {

        double factor;

        if (isPrime(targetNo)) {
            System.out.println("Prime factor = " + targetNo);
            return targetNo;
        }
      
        Double aPrime = primes[primeIndex];
 
        if (isTargetDivisibleByPrime(targetNo, aPrime)) {
            targetNo = findNewTarget(targetNo, aPrime);
            System.out.println("Prime factor = " + aPrime);
            factor = primeFactorise(targetNo, 0);
        } else {
            factor = primeFactorise(targetNo, primeIndex + 1);
        }

        return aPrime;
    }

    private boolean isTargetDivisibleByPrime(Double targetNo, Double aPrime) {
        double fractionalPart = getFractionalPart(targetNo / aPrime);
        return fractionalPart == 0.0d;
    }

    private double findNewTarget(Double targetNo, Double aPrime) {
        return targetNo / aPrime;
    }

    private boolean isPrime(Double targetNo) {
        for (Double prime : primes) {
            if(targetNo.equals(prime)){
                return true;
            }
        }
        return false;
    }

    private static double getFractionalPart(double n) {
        if (n > 0) {
            return n - Math.floor(n);
        } else {
            return ((n - Math.ceil(n)) * -1);
        }
    }
    
    public static boolean isPrime(int number) {
        for(int check = 2; check < number; ++check) {
            if(number % check == 0) {
                return false;
            }
        }
        return true;
    }

    private void createPrimes() {
        primes = IntStream.rangeClosed(2, 100000)
                .filter(n -> isPrime(n))
                .mapToDouble(n->n)
                .toArray();
              
    }

}
