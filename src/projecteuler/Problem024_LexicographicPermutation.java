/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecteuler;

import java.util.Arrays;

/**
 *
 * Lexicographic permutations Problem 24
 *
 * A permutation is an ordered arrangement of objects. For example, 3124 is one
 * possible permutation of the digits 1, 2, 3 and 4. If all of the permutations
 * are listed numerically or alphabetically, we call it lexicographic order. The
 * lexicographic permutations of 0, 1 and 2 are: 012 021 102 120 201 210 What is
 * the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7,
 * 8 and 9?
 */
public class Problem024_LexicographicPermutation {

    /**
     * The algorithm is quite straightforward and may be memorized:
     *
     * Find the biggest i such that a[i] < a[i + 1];
     * Find the biggest j greater than i such that a[j] > a[i]; Swap a[i] and
     * a[j]; Reverse the elements from a[i + 1] to the last element.
     *
     * If the first step fails (because such index does not exist) the current
     * permutation is the last one.
     *
     * Given any permutation of a list, this generates the next one. It should
     * be noted, however, that when given the greatest lexicographic
     * permutation, this algorithm returns this same permutation, so it should
     * be checked to ensure that if the permutation at hand is the last one, we
     * reverse the sequence to get back to the first permutation.
     */
    public static void main(String[] args) {
        new Problem024_LexicographicPermutation().run();
    }

    private void run() {

        String perm = "0123456789";
        System.out.println("  Perm = " + perm);

        String prevPerm = null;
        for (int i = 1; i < 1000000; i++) {

            int firstCharPos = findFirstCharPos(perm);
            int secondCharPos = secondCharPos(perm, firstCharPos);
            perm = swap(perm, firstCharPos, secondCharPos);
            perm = postSwapSort(perm, firstCharPos);

            if (perm.equals(prevPerm)) {
                break;
            }
            
            if(i == 999999){
                System.out.println(i + " Perm = " + perm);
            }
            prevPerm = perm;

        }

    }

    // Take the previously printed permutation and find the rightmost character
    // in it, which is smaller than its next character. 
    private int findFirstCharPos(String perm) {

        int firstCharPos = -1;

        for (int pos = 0; pos < perm.length() - 1; ++pos) {
            int curPos = Character.getNumericValue(perm.charAt(pos));
            int nextPos = Character.getNumericValue(perm.charAt(pos + 1));
            if (curPos < nextPos && pos > firstCharPos) {
                firstCharPos = pos;
            }
        }
        return firstCharPos;
    }

    // Now find the ceiling of the ‘first character’. Ceiling is the smallest 
    // character on right of ‘first character’, which is greater than ‘first character’. 
    private int secondCharPos(String perm, int firstCharPos) {

        int secondCharPos = firstCharPos + 1;
        int minSecondChar = Character.getNumericValue(perm.charAt(secondCharPos));

        for (int pos = firstCharPos + 1; pos < perm.length(); ++pos) {
            int secondChar = Character.getNumericValue(perm.charAt(pos));
            int firstChar = Character.getNumericValue(perm.charAt(firstCharPos));

            if (secondChar > firstChar && secondChar < minSecondChar) {
                minSecondChar = secondChar;
                secondCharPos = pos;
            }
        }
        return secondCharPos;
    }

    // Swap first and second characters.
    private String swap(String perm, int firstCharPos, int secondCharPos) {
        StringBuilder str = new StringBuilder(perm);
        char tmp = str.charAt(firstCharPos);
        str.setCharAt(firstCharPos, str.charAt(secondCharPos));
        str.setCharAt(secondCharPos, tmp);
        return str.toString();
    }

    // Sort the substring (in non-decreasing order) after the original index of ‘first character’.
    public String postSwapSort(String perm, int firstCharPos) {
        String front = perm.substring(0, firstCharPos + 1);
        String back = perm.substring(firstCharPos + 1);
        
        char[] arr = back.toCharArray();
        Arrays.sort(arr); 
        
        return front + new String(arr);

    }

}
