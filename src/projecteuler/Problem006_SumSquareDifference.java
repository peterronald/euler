/*

Sum square difference
Problem 6 

The sum of the squares of the first ten natural numbers is,
    1pow2 + 2pow2 + ... + 10pow2 = 385

The square of the sum of the first ten natural numbers is,
    (1 + 2 + ... + 10)2 = 552 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

 */
package projecteuler;

import java.util.stream.IntStream;

/**
 *
 * @author Peter
 */
public class Problem006_SumSquareDifference {

    public static void main(String[] args) {
        new Problem006_SumSquareDifference().run();
    }

    private void run() {

        // Sum of the squares.
        double sumOfSquares = IntStream.rangeClosed(1, 100)
                .mapToDouble(n -> Math.pow(n, 2d))
                .sum();

        // Square of the sum.
        int sum = IntStream.rangeClosed(1, 100).sum();
        double squareOfSum = Math.pow(sum, 2d);

        // The difference.
        double difference = squareOfSum - sumOfSquares;
        System.out.println("Difference = " + difference);
    }
}
