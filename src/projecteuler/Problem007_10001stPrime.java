/*
10001st prime
Problem 7

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?

 */
package projecteuler;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author Peter
 */
public class Problem007_10001stPrime {
    
    public static void main(String[] args) {
        new Problem007_10001stPrime().run();
    }

    private void run() {
 
        Predicate<Integer> isOdd = x -> x % 2 != 0;
        
//        List<Integer> primes = IntStream
//                .rangeClosed(2, 125000)
//                .boxed()
//                .filter(n -> isPrime(n))
//                .collect(Collectors.toList());
//      System.out.println("10001st prime = " + primes.get(10000));
        
        List<Integer> primes = findSeivePrimes(100);
        System.out.println("10001st prime = " + primes.get(100));
    }
                
     public boolean isPrime(int number) {
         if (number == 2){
             return true;
         }
         if (number % 2 == 0) {
             return false;
         }
        for(int check = 2; check < number; ++check) {
            if(number % check == 0) {
                return false;
            }
        }
        return true;
    }       
     

    public List<Integer> findSeivePrimes(int primesUpToNum) {
        List<Integer> marked = new ArrayList();
        List<Integer> primes = new ArrayList();
        
        for (int i = 2; i < Math.sqrt((double)primesUpToNum); i++) {
            if(!marked.contains(i)) {
                primes.add(i);
            } 
            for (int j = i; j < primesUpToNum; j += i ) {
                marked.add(j);
            }
        }
        return primes;
    }
}
