/*
Largest palindrome product
Problem 4

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

 */
package projecteuler;

import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;

/**
 *
 * @author Peter
 */
public class Problem004_LargestPalindromeProduct {
    
     public static void main(String[] args) {
        new Problem004_LargestPalindromeProduct().run();
    }

    private void run() {
         
        List<Integer> palindromes = new ArrayList();
        IntStream.range(111, 999) 
            .sequential() 
               .forEach( (i) ->  { 
                  IntStream.range(111, 999)
                            .sequential() 
                            .forEach( (j) -> { 
                               if(isPalindrome(i*j)) {
                                   palindromes.add(i*j);
                                   System.out.println("Adding " + i*j);
                               }
                            
                            }); 
                          }); 
         
         OptionalInt max = palindromes.stream().mapToInt(Integer::intValue).max();
         System.out.println("Largest palindrome made from the product of two 3-digit numbers = " + max.getAsInt());
    }
    
    
    private boolean isPalindrome(int num) {
        String numStr = String.valueOf(num);
        String reverseNumStr = new StringBuilder(numStr).reverse().toString();
//        if(numStr.equals(reverseNumStr))
//            System.out.println("isPalindrome() " + num + " reverse" + reverseNumStr);
        return numStr.equals(reverseNumStr);
    }
}
