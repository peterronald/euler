/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecteuler;

//Problem 26 
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

//  A unit fraction contains 1 in the numerator. 
//  The decimal representation of the unit fractions with denominators 2 to 10 are given:
//
//  1/2	= 	0.5(0)
//  1/3	= 	0.(3)
//  1/4	= 	0.25
//  1/5	= 	0.2
//  1/6	= 	0.1(6)
//  1/7	= 	0.(142857)
//  1/8	= 	0.125(0)
//  1/9	= 	0.(1)
//  1/10	= 	0.1
//
//  Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. 
//  It can be seen that 1/7 has a 6-digit recurring cycle.
//
//Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
// 
// Recurring fraction a/b will recur in less than b places.
public class Problem026_ReciprocalCycles {

    List<SubCalc> subCalcs = new ArrayList();

    public Problem026_ReciprocalCycles() {
    }

    public static void main(String[] args) {
        Problem026_ReciprocalCycles cycles = new Problem026_ReciprocalCycles();
        cycles.isRecurring(6);
    }

    boolean isRecurring(int denominator) {

        StringBuilder sb = new StringBuilder();

        int numerator = 10;
        SubCalc subCalc;

        while (true) {

            subCalc = new SubCalc();

            while (denominator > numerator) {
                numerator *= 10;
                subCalc.decimalPart = "0";
            }

            subCalc.remainder = numerator % denominator;
            subCalc.decimalPart += String.valueOf(numerator / denominator);
            subCalcs.add(subCalc);
             
            int splitPos = findRepeatedRemainderPosition(subCalc);
            if (splitPos >= 0) {
                
                String reciprocal = getReciprocal(splitPos);
                String front = getFront(splitPos);
                System.out.println("Denominator is "+ denominator + " is recurring length=" + reciprocal.length() + " (0." + front + reciprocal + ")");

                return true;
            } 

 
            numerator = subCalc.remainder * 10;

            if (numerator == 0) {
                return true;
            }

        }

    }
    
 
    private int findRepeatedRemainderPosition(SubCalc lastSubCalc) {
        int pos = -1;
        if(subCalcs.size() < 2 ) return pos;

        for (SubCalc sc : subCalcs) {
            pos++;
            if(sc.remainder == lastSubCalc.remainder) {
                if(!sc.equals(lastSubCalc)){
                    return pos;
                } else {
                    return -1;
                }
            }
        }
        
        return -1;
    }

    private String getReciprocal(int splitPos) {
        return (subCalcs.subList(splitPos,subCalcs.size()))
                .stream()
                .map(i -> i.toString())
                .collect(Collectors.joining(""));
    }

    private String getFront(int splitPos) {
        return (subCalcs.subList(0, splitPos))
                .stream()
                .map(i -> i.toString())
                .collect(Collectors.joining(""));
    }

    private class SubCalc {

        public int remainder;
        public String decimalPart = "";

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 43 * hash + this.remainder;
            hash = 43 * hash + Objects.hashCode(this.decimalPart);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final SubCalc other = (SubCalc) obj;
            if (this.remainder != other.remainder) {
                return false;
            }
            if (!Objects.equals(this.decimalPart, other.decimalPart)) {
                return false;
            }
            return true;
        }

      

    }


}
