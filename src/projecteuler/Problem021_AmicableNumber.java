/*
Amicable numbers
Problem 21

Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. 
The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.

 */
package projecteuler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

/**
 *
 * @author Peter Amicable numbers are two different numbers so related that the
 * sum of the proper divisors of each is equal to the other number.
 *
 * The first ten amicable pairs are: (220, 284), (1184, 1210), (2620, 2924),
 * (5020, 5564), (6232, 6368), (10744, 10856), (12285, 14595), (17296, 18416),
 * (63020, 76084), and (66928, 66992).
 */
public class Problem021_AmicableNumber {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        new Problem021_AmicableNumber().run();

    }

    private void run() {
        Map<Integer, Integer> factors = new HashMap();
        IntStream.rangeClosed(1, 10000).boxed()
                .forEach(k -> factors.put(k, sumDivisors(findDivisors((int) k))));
        factors.entrySet().stream().forEach(System.out::println);

        List<Integer> amicables = new ArrayList();
        factors.entrySet().stream().forEach(e1 -> {

            Integer key1 = e1.getKey();
            Integer value1 = e1.getValue();
            Integer value2 = factors.get(value1);

            if (key1.equals(value2) && !key1.equals(value1)) {
                amicables.add(key1);
            }

        });
        System.out.println("Sum amicable numbers = " + amicables.stream().reduce(0, (a, b) -> a + b));
    }

    private static int[] findDivisors(int num) {
        return IntStream.range(1, 10000)
                .filter(n -> n != num)
                .filter(n -> num % n == 0)
                .toArray();
    }

    private static int sumDivisors(int[] divisors) {
        return Arrays.stream(divisors)
                .sum();
    }

//    private List<Pair> createProduct(List<Integer> list1, List<Integer> list2) {
//        return list1.stream()
//                        .flatMap(e1 -> list2.stream()
//                                .map(e2 -> new Pair(e1, e2))
//                        ).collect(Collectors.toList());
//                
//    }
}
