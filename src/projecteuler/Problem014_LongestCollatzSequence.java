/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projecteuler;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * The following iterative sequence is defined for the set of positive integers:
 *
 * n → n/2 (n is even) n → 3n + 1 (n is odd)
 *
 * Using the rule above and starting with 13, we generate the following
 * sequence:
 *
 * 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1 
 * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. 
 * Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers
 * finish at 1.
 *
 * Which starting number, under one million, produces the longest chain?
 *
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 */
public class Problem014_LongestCollatzSequence {

    static List<BigInteger> maxChain = Collections.EMPTY_LIST;
    private static int count = 0;

    public Problem014_LongestCollatzSequence() {

        
        Map<Integer, Integer> chainSizeMap  = IntStream.range(1, 1000000).boxed()
                .collect(Collectors.toMap(n -> n, n -> getChain(n).size()));
        
        
//        // For each starting number create the chain.
//        Map<Integer, List<BigInteger>> chainMap = IntStream.range(1, 10).boxed()
//                .collect(Collectors.toMap(n -> n, n -> getChain(n)));
//        chainMap.entrySet().stream().forEach(System.out::println);
//
//        // For each starting number create the chain SIZE.
//        Map<Integer, Integer> chainSizeMap = chainMap.entrySet().stream()
//                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().size()));
//        chainSizeMap.entrySet().stream().forEach(System.out::println);

        // Find and display longest chain size.
        Integer maxChainLength = Collections.max(chainSizeMap.values());
        System.out.println("Longest chain length = " + maxChainLength);
        
        // Find and display longest chain starting number (reverse lookup).
        List<Integer> startingNumbers = chainSizeMap.entrySet().stream()
                .filter(entry -> maxChainLength.equals(entry.getValue()))
                .map(map -> map.getKey())
                .collect(Collectors.toList());
        System.out.println("Longest chain starting number = " + startingNumbers);       
        
        
        
//        startingNumbers.forEach(key -> {
//            List<Map.Entry<Integer, List<BigInteger>>> collect = chainMap.entrySet().stream()
//                    .filter(entry -> key.equals(entry.getKey()))
//                    .collect(Collectors.toList());
//        });
        
    }

    private List<BigInteger> getChain(int startNo) {

        List<BigInteger> chain = new ArrayList();
        BigInteger currentNo = BigInteger.valueOf(startNo);

        //System.out.println(startNo);
        chain.add(currentNo);
        do {
            currentNo = calcNextSequenceNo(currentNo);
            chain.add(currentNo);
        } while (!currentNo.equals(BigInteger.ONE));

        return chain;
    }

    private BigInteger calcNextSequenceNo(BigInteger currentNo) {
        boolean isEven = currentNo.mod(BigInteger.valueOf(2)).equals(BigInteger.ZERO);
        if (isEven) {
            return currentNo.divide(BigInteger.valueOf(2));
        } else {
            return currentNo.multiply(BigInteger.valueOf(3)).add(BigInteger.valueOf(1));
        }
    }

    public static void main(String[] args) {
        new Problem014_LongestCollatzSequence();
    }

}
