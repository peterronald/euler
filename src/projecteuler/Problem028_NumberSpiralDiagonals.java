/*
Number spiral diagonals
Problem 28

Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101. (21+7+1+3+13) + (25+9+1+5+17)

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?

 */
package projecteuler;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Peter
 */
public class Problem028_NumberSpiralDiagonals {

    List<Integer> diagonals = new ArrayList();
    List<Integer> topRightIncrements = new ArrayList();
    int topRightOffset = 8;
    int bottomRightOffset = 6;
    int bottomLeftOffset = 4;
    int topLeftOffset = 2;
   
    public static void main(String[] args) {
        new Problem028_NumberSpiralDiagonals().run();
    }

    private void run() {

        final int CENTER_VALUE = 1;

        // Calc all diagonals.
        topRightIncrements.add(CENTER_VALUE);
        for (int i = 1; i < 501; i++) {
            Optional<Integer> value = calcTopRightOfSquare(i);
            diagonals.add(value.get() - (i * bottomRightOffset));
            diagonals.add(value.get() - (i * bottomLeftOffset));
            diagonals.add(value.get() - (i * topLeftOffset));
            diagonals.add(value.get());
        }
        diagonals.forEach(System.out::println);
        
        Optional<Integer> sumAll = diagonals.stream().reduce((x, y) -> x + y);
        System.out.println("Total of all diagonals = " +  sumAll.get() + CENTER_VALUE);

    }

    private Optional<Integer> calcTopRightOfSquare(int i) {
        topRightIncrements.add(topRightOffset * i);
        return topRightIncrements.stream().reduce((x, y) -> x + y);
    }

}
