/*
Counting Sundays
Problem 19

You are given the following information, but you may prefer to do some research for yourself.

    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.
    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

 */
package projecteuler;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.stream.IntStream;

/**
 *
 * @author Peter
 */
public class Problem019_CountingSundays {

    public static void main(String[] args) {
        new Problem019_CountingSundays().run();
    }

    private void run() {

        LocalDate startDate = LocalDate.parse("1900-12-31");
        LocalDate endDate = LocalDate.parse("2000-12-31");

        long days = startDate.until(endDate, ChronoUnit.DAYS);
        long count = IntStream.rangeClosed(1, (int) days)
                .boxed()
                .map(d -> startDate.plusDays(d))
                .filter(d -> isFirstOfTheMonth(d) && isSunday(d))
                .count();

        System.out.println("Sundays fell on the first of the month = " + count);
    }

    private boolean isSunday(LocalDate date) {
        return date.getDayOfWeek().compareTo(DayOfWeek.SUNDAY) == 0;
    }

    private boolean isFirstOfTheMonth(LocalDate date) {
        return date.getDayOfMonth() == 1;
    }
}
